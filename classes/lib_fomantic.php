<?php

/**
 *  @module      	Library Semantic
 *  @version        see info.php of this module
 *  @author         LEPTON project
 *	@copyright      2014-2023 LEPTON Project
 *  @license        https://opensource.org/licenses/MIT
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

class lib_semantic extends LEPTON_abstract
{

    const BASEPATH = LEPTON_PATH."/modules/lib_semantic/dist/";
    
    // Own instance for this class!
    static $instance;
    
    public function initialize()
    {
    
    }
}
